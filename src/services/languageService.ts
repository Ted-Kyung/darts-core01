import { Component } from '@angular/core';

// using event api
import { Platform } from 'ionic-angular';

@Component({
  selector: null,
  templateUrl: null
})



export class LangServices {
  Ko = {

  }
  
  En = {
  
  }
  
  Ja = {
  
  }
  LANG_DATA = {
    ko: this.Ko,
    en: this.En,
    ja: this.Ja
  }

  constructor(private platform: Platform) {
    this.platform.ready();
  }

  getLangData(key, defaultValue) {
    let langCode = window.navigator.language.substr(0, 2).toLowerCase();
    let langData = this.LANG_DATA[langCode];

    if (!!langData[key]) {
      return langData[key];
    }

    return defaultValue;
  }

  getLang() {
    return window.navigator.language.substr(0, 2).toLowerCase();
  }
}