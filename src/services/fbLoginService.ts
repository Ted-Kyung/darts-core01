import { Component } from '@angular/core';

// using event api
import { LoadingController,AlertController } from 'ionic-angular';

// firebase login
// import { Facebook } from '@ionic-native/facebook';

import * as firebase from 'firebase';

// lang service 
import { LangServices } from '../services/languageService';

/**
 how to make service class ???
 @Component <= required to this comment below
 import to app.module.ts
 add to app.module.ts @NgModule..provider []
 */
@Component({
  selector: null,
  templateUrl: null
})
export class FirebaseLoginService {

  UserBasePath = '/userItems/';
  storageLoginItems = 'loginItems';
  PATH_CODE = "";
  USERITEM= null;

  langData = {};

  constructor(private lang:LangServices,
              private alertCtrl:AlertController,
              private loadingCtrl: LoadingController,){
              // private facebook: Facebook) {
    this._setLanguage();
  }

  _setLanguage(){
    this.langData = {
      OK: this.lang.getLangData('OK', "OK"),
      cancel: this.lang.getLangData('cancel', "Cancel"),
      convertEmailToFacebook: this.lang.getLangData('convertEmailToFacebook', 'Need your Password for convert Facebook ID'),
      loginErrMsgReCheck: this.lang.getLangData('loginErrMsgReCheck', 'Please try again'),
      loginErrMsgInvaildEmail: this.lang.getLangData('loginErrMsgInvaildEmail', "Can't found E-mail"),
      loginErrMsgInvalidPass: this.lang.getLangData('loginErrMsgInvalidPass', 'Invalid Password'),
      sendConfirmEmail: this.lang.getLangData('sendConfirmEmail', 'Confirm E-mail'),
      sendConfirmEmailDesc: this.lang.getLangData('sendConfirmEmailDesc', 'Please Check your E-mail'),
      notYetConfirmEmailDesc: this.lang.getLangData('notYetConfirmEmailDesc', 'Need to E-mail Verified'),
      facebookHasNotMail: this.lang.getLangData('facebookHasNotMail', 'Facebook Account has not e-mail address'),
    }
  }

  // registration progress
  showRegesterFacebookUserAlert(){
    let loading = this.loadingCtrl.create({
        spinner: 'crescent',
        content: 'Create Account ..',
        duration: 2000
      });
    return loading;
  }

  // login progress 
  showLoginProgress(){
    let loading = this.loadingCtrl.create({
        spinner: 'crescent',
        content: 'Login ..',
        duration: 2000
      });
    return loading;
  }

  // save local storage reason automation login
  // saveLoginUserItem(item){
  //   this.storage.set(this.storageLoginItems, item);
  // }

  // // load useritems from local storage
  // loadLoginUserItem():Promise<any>{
  //   return new Promise((resolve, reject)=>{
  //     this.storage.get(this.storageLoginItems).then((res)=>{
  //       if(res){
  //         resolve(res);
  //       }else{
  //         reject();
  //       }
  //     });
  //   });
  // }

  // // remove useritems, usecase: logout! 
  // removeLoginUserItem(){
  //   this.PATH_CODE = "";
  //   this.USERITEM = null;
  //   firebase.auth().signOut();
  //   this.storage.remove(this.storageLoginItems);
  // }

  // show alert login failed reason 
  showLoginFailedAlert(error){
      var err_char = '';
      var err_desc = '';
      if((error['code'] == 'auth/user-not-found') || (error['code'] == 'auth/invalid-email')){
        err_char = this.langData['loginErrMsgInvaildEmail'];
        err_desc = this.langData['loginErrMsgReCheck'];
      } else if (error['code'] == 'auth/wrong-password') {
        err_char = this.langData['loginErrMsgInvalidPass'];
        err_desc = this.langData['loginErrMsgReCheck'];
      }
      else if (error['code'] == 'notEmail') {
        err_char = this.langData['loginErrMsgInvaildEmail'];
        err_desc = this.langData['facebookHasNotMail'];
      }
      console.log("error : ", error['code'])
      let alert = this.alertCtrl.create({
          title: err_char,
          subTitle: err_desc,
          buttons: ['OK']
        });
      alert.present();
  }

  // update password, using into convert email to facebook
  updatePassword(pass):Promise<any>{
    return new Promise((resolve, reject)=>{
      var user = firebase.auth().currentUser;
      user.updatePassword(pass).then((res)=>{
        resolve(res);
      }).catch((error)=>{
        reject(error);
      });
    });
  }

  // registrationAccountToDatabase(userItem, displayName):Promise<any>{
  //   return new Promise((resolve, reject)=>{
  //     // var usrItem = new UserItem();
  //     var usrItem;
  //     usrItem.uid = userItem.uid;
  //     usrItem.uidRefreshToken = userItem.refreshToken;
  //     usrItem.email = userItem.email;
  //     usrItem.user_name = displayName;
  //     this.db.setDataWithTokenStyle(this.UserBasePath, usrItem.uid, usrItem).then((res)=>{
  //       resolve(res);
  //     });
  //   });
  // }

  // if first registration from facebook...
  generateFromFaceBookInfo(lgEmail, lgPass, displayName):Promise<any>{
    return new Promise((resolve, reject)=>{
      var progress = this.showRegesterFacebookUserAlert();
      progress.present();
      this.createAccountWithEmail(lgEmail, lgPass, displayName)
        .then((cRes)=>{
          resolve(cRes);
        })
        .catch((cError)=>{
          progress.dismiss();
          reject(cError);
      });
    });
  }

  // send to verification email
  sendEmailVerification(user):Promise<any>{
    return new Promise((resolve, reject)=>{
      firebase.auth().languageCode = this.lang.getLang();
      user.sendEmailVerification().then((res)=>{
        console.log("res : " + JSON.stringify(res));
        resolve(res);
      }).catch(function(error) {
        console.error("error : " + error);
        reject(error);
      });
    });
  }
  // if user account email not yet verification   
  showEmailIsNotVerified(user){
    var sendCheckEmailConfirmAlert = this.alertCtrl.create({
      title: this.langData['sendConfirmEmail'],
      subTitle:this.langData['notYetConfirmEmailDesc'],
      buttons: [{
          text: this.langData['cancel'],
          handler: () => {}
        },
        {
          text: this.langData['OK'],
          handler: () => {
            console.log("showEmailIsNotVerified : " + ", " + JSON.stringify(user));
            this.sendEmailVerification(user).then((res)=>{}).catch((err)=>{});
          }
        }]
    });
    sendCheckEmailConfirmAlert.present();
  }

  // if registration account, send to Verification email
  showPleaseConfirmUserEmailAlert(user):Promise<any>{
    return new Promise((resolve, reject)=>{
      console.log("!!!!!!!!! start showConfirmUserEmail");
      var sendCheckEmailConfirmAlert = this.alertCtrl.create({
        title: this.langData['sendConfirmEmail'],
        subTitle:this.langData['sendConfirmEmailDesc'],
        buttons: [{
            text: this.langData['OK'],
            handler: () => {}
          }]
      });
      sendCheckEmailConfirmAlert.onDidDismiss(()=>{
        firebase.auth().languageCode = this.lang.getLang();
        console.log("showConfirmUserEmail : " + ", " + JSON.stringify(user));
        this.sendEmailVerification(user).then((res)=>{resolve(res);}).catch((error)=>{reject(error);});
      });
      sendCheckEmailConfirmAlert.present();
    });
  }

  // convert Email to facebook flow
  // try login original email with password
  // change password to facebook user acccount id after email login
  // retry email login with password(facebook id)
  convertEmailToFaceBookPassword(lgEmail, lgPass):Promise<any>{
    return new Promise((resolve, reject)=>{
      var convertAlert = this.alertCtrl.create({
        title: this.langData['convertEmailToFacebook'],
        inputs: [{
            name: 'password',
            placeholder: 'Password',
            type: 'password'
          }],
        buttons: [{
            text: this.langData['cancel'],
            role: 'cancel',
            handler: data => {
              console.log('Cancel clicked');
            }
          },
          {
            text: this.langData['OK'],
            handler: data => {
              this.loginWithEmail(lgEmail, data.password).then((res)=>{
                this.updatePassword(lgPass);
                resolve(res);
              }).catch((error)=>{
                console.error(error);
                reject(error);
              });
            }
          }]
      });
      convertAlert.present();
    });
  }

  // // get facebook user items, email, name, etc ... 
  // public getFacebookAccoutItem():Promise<any>{
  //   return new Promise((resolve, reject)=>{
  //     this.facebook.api("me/?fields=id,name,email,birthday,gender", ['public_profile', 'email'])
  //     .then(res=>{
  //       console.log("fb attach Success : " + JSON.stringify(res));
  //       resolve(res);
  //     })
  //     .catch(error=>{
  //       console.log("fb attach Error : " + JSON.stringify(error));
  //       reject(error);
  //     });
  //   });
  // }

  private isAlreadyRegistrationEamil(error){
    return (error['code'] == 'auth/wrong-password');
  }

  private isFirstTimeLoginFromFacebook(error){
    return (error['code'] == "auth/user-not-found");
  }

  // // try facebook login, attach public infomation & email address,
  // // if has not email address to return error
  // // if already registration email address to try convert facebook account
  // // if can't found email address to create account
  // public facebookLogin(): Promise<any> {
  //   return new Promise((resolve, reject)=>{
  //     this.facebook.login(['public_profile', 'email']).then( response => {
  //       let fbAuthResp = response.authResponse;
  //       this.getFacebookAccoutItem().then(res=>{
  //         if(!res.hasOwnProperty("email")){
  //           this.showLoginFailedAlert({code:"notEmail"});
  //           reject("notEmail");
  //         }
  //         let lgEmail = res.email;
  //         let lgPass = fbAuthResp.userID;
  //         this.loginWithEmail(lgEmail, lgPass, 'facebook').then(lRes=>{
  //           resolve(lRes);
  //         }).catch(lError=>{
  //           if(this.isAlreadyRegistrationEamil(lError)){
  //             this.convertEmailToFaceBookPassword(lgEmail, lgPass).then((res)=>{
  //               resolve(res);
  //             }).catch((err)=>{
  //               console.error("error: " + err);
  //               reject(err);
  //             });
  //           } else if (this.isFirstTimeLoginFromFacebook(lError)){
  //             this.generateFromFaceBookInfo(lgEmail, lgPass, res.name).then((res)=>{
  //               resolve(res);
  //             }).catch((err)=>{
  //               console.error("error : " + err);
  //               reject(err);
  //             });
  //           }
  //         });
  //       }).catch(error=>{
  //         console.log("fb attach Error:" + JSON.stringify(error));
  //         reject(error);
  //       });
  //     });
  //   });
  // }

  // try reset account password
  public resetPasswardEmailSend(email:string):Promise<any>{
    return new Promise((res, rej)=>{
      firebase.auth().sendPasswordResetEmail(email).then(r=>{res(r);}).catch(e=>{rej(e);})
    });
  }

  // login with email address, but try from facebook, don't show error alert 
  // and management login progress modal
  public loginWithEmail(email: string, pass: string, from=null): Promise<any> {
    return new Promise((resolve, reject)=>{
      var progress = this.showLoginProgress();
      progress.present();
      firebase.auth().signInWithEmailAndPassword(email, pass).then(response => {
        let user = firebase.auth().currentUser;
        resolve(user);
      }).catch(error => {
        reject(error);
      });
    });
  }

  // Check email athented , if login to test account pass this checker
  public checkAuthentedEmailUser(user):Promise<any>{
    return new Promise((resolve, reject)=>{
      var domain = String(user.email).split('@')[1];
      if(domain == 'test.com'){
        resolve();
      }
      if(user.emailVerified){
        resolve();
      }else{
        reject();
      }
    });
  }

  // return useritem of last login user 
  public getFbCurrentUserItem(){
    console.log("email : " + firebase.auth().currentUser.email);
    return firebase.auth().currentUser;
  }

  public removeUserAccount(email: string, pass: string):Promise<any>{
    return new Promise((resolve, reject)=>{
      this.loginWithEmail(email, pass)
      .then(res=>{
        let user = firebase.auth().currentUser;
        user.delete().then((res)=>{
          resolve();
        })
        .catch((err)=>{
          reject(err);
        });
      })
      .catch(err=>{
        reject(err);
      });
    });
  }

  // create email account to firebase database
  public createAccountWithEmail(email: string, pass: string, displayName:string):Promise<any>{
    return new Promise((resolve, reject) => {
      console.log("email: " + email);
      firebase.auth().createUserWithEmailAndPassword(email, pass).then(response => {
        console.log("got response : ", response);
        var user = firebase.auth().currentUser;
        resolve(user);
      }).catch(error => {
        console.log("got error : ", error);
        reject(error);
      });
    });
  }
 
  // // check login user item, and try login into automation 
  // public automationLoginSystem():Promise<any>{
  //   return new Promise((resolve, reject)=>{
  //     console.log("try automation login");
  //     this.loadLoginUserItem().then((res)=>{
  //       this.loginWithEmail(res.email, res.pw).then((res)=>{
  //         resolve(res);
  //       }).catch((error)=>{
  //         this.removeLoginUserItem();
  //         reject(error);
  //       });
  //     }).catch((error)=>{
  //       reject(error);
  //     });
  //   });
  // }

  // public getCurrentUserItem(){
  //   return new Promise((resolve, reject)=>{
  //     let userId = this.getCurrentUserId();
  //     if(!!this.USERITEM){
  //       resolve(this.USERITEM);
  //     } else {
  //       if(!!userId){
  //         this.db.getItemWithPathAndKey(this.UserBasePath, userId)
  //         .then((res)=>{
  //           this.USERITEM = res;
  //           resolve(this.USERITEM);
  //         })
  //         .catch((error)=>{
  //           reject(error);
  //         });
  //       }
  //       else{
  //         reject('nullId');
  //       }
  //     }
  //   });
  // }

  public getCurrentUserId(){
    return firebase.auth().currentUser.uid;
  }

  // public getCurrentUserCode():Promise<any>{
  //   return new Promise((resolve, reject)=>{
  //     let userId = this.getCurrentUserId();
  //     var returnCode = "";
  //     if(!!this.PATH_CODE){
  //       resolve(this.PATH_CODE);
  //     } else {
  //       if(!!userId){
  //         this.db.getItemWithPathAndKey(this.UserBasePath, userId)
  //         .then((res)=>{
  //           if(res.tester){
  //             returnCode = 'test';
  //           } else {
  //             returnCode = this.lang.getLang();
  //           }
  //           this.PATH_CODE = returnCode;
  //           resolve(returnCode);
  //         })
  //         .catch((error)=>{
  //           reject(error);
  //         });
  //       }
  //       else{
  //         reject('nullId');
  //       }
  //     }
  //   });
  // }
}
