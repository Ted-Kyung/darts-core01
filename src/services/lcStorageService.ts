import { Component } from '@angular/core';

// using event api
import { Platform } from 'ionic-angular';

import { Storage } from '@ionic/storage';

/*
Installations
ionic cordova plugin add cordova-sqlite-storage --save ; npm install --save @ionic/storage;

import { IonicStorageModule } from '@ionic/storage';
@NgModule({
  declarations: [
    // ...
  ],
  imports: [
    // ...
    IonicStorageModule.forRoot()
*/

@Component({
  selector: null,
  templateUrl: null
})

export class LocalStorageService {

  constructor(private platform: Platform,
              private storage: Storage) {
    this.platform.ready();
  }

  public async setLocalStorage(key:string, value){
    try{
      await this.storage.set(key, value);
      return true;
    }
    catch{
      return false;
    }
  }
  
  public async getLocalStorage(key: string){
    try{
      let _res = await this.storage.get(key);
      return _res;
    }
    catch{
      return null;
    }
  }

  public async removeLocalStorage(key: string){
    try{
      let _res = await this.storage.remove(key);
      return _res;
    }
    catch{
      return null;
    }
  }
}