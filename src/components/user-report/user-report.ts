import { Component, EventEmitter, Input, Output } from '@angular/core';

import { FirebaseRestApiServices } from "../../services/fbRestApi";

import { LoadingController, Platform } from 'ionic-angular';

import { StatusBar } from '@ionic-native/status-bar';

import echarts from 'echarts/lib/echarts';

import { AdMobFree, AdMobFreeBannerConfig, AdMobFreeInterstitialConfig } from '@ionic-native/admob-free';

@Component({
  selector: 'user-report',
  templateUrl: 'user-report.html'
})
export class UserReportComponent {
  @Input() userId:string;
  @Output() closeCallback = new EventEmitter();

  openDetailReport = false;
  detailGame = null;

  getGameLength = 30;
  getDateLength = 7;

  reportData = null;
  options = {
    textStyle: {
      color: "#FFFFFF",  
    },
    tooltip: {
        trigger: 'axis',
        position: function (pt) {
            return [pt[0], '100%'];
        }
    },
    xAxis: {
        type: 'category',
        boundaryGap: false,
        data: []
    },
    yAxis: {
        type: 'value',
        boundaryGap: [0, '100%']
    },
    dataZoom: [{
        type: 'inside',
        start: 0,
        end: 100
    }],
    series: [
        {
            name:'Average PPR',
            type:'line',
            smooth:true,
            symbol: 'none',
            sampling: 'average',
            itemStyle: {
                color: 'rgb(255, 70, 131)'
            },
            areaStyle: {
                color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
                    offset: 0,
                    color: 'rgb(255, 158, 68)'
                }, {
                    offset: 1,
                    color: 'rgb(255, 70, 131)'
                }])
            },
            data: []
        }
    ]
};

_echart = null;

  constructor(private restApi: FirebaseRestApiServices,
              public admob: AdMobFree, 
              public platform: Platform,
              public statusBar:StatusBar,
              private loadingCtrl: LoadingController) {
    

  }

  async showBanner() {

    var code = "";
    if(this.platform.is("ios")){
      console.log("NNNNNNNN IOS");
      let _code = await this.restApi.restApiWithData('getAdmobId', {os:'ios'})
      code = _code.result;
    } else if(this.platform.is("android")){
      console.log("NNNNNNNN ANDROID");
      let _code = await this.restApi.restApiWithData('getAdmobId', {os:'android'})
      code = _code.result;
    }

    console.log("code : " + code);
    let bannerConfig: AdMobFreeInterstitialConfig = {
        isTesting: false,
        autoShow: true,
        id:code
    };
    this.admob.interstitial.config(bannerConfig);
    this.admob.interstitial.prepare().then(() => {
    }).catch(e => console.log(e));
  }

  updateOptions(recentPPRs){
    console.log("update!!!");
    this.options.series[0].data = recentPPRs;
    var xAxis = [];
    recentPPRs.forEach((element, index) => {
      xAxis.push(index);
    });
    this.options.xAxis.data = xAxis;

    this._echart.setOption(this.options);
  }

  ngOnInit(){
    this._echart = echarts.init(document.getElementById("echart"));
    this._echart.setOption(this.options);

    this.getUserReport();
    this.showBanner();
  }

  private _showProgress(timeout:number){
    let loading = this.loadingCtrl.create({
      spinner: 'crescent',
      duration: timeout
    });
    loading.present();
    return loading;
  }

  public async getUserReport(){
    console.log("loaded!!");
    var userReport = null;
    this.reportData = null;
    let loading = this._showProgress(10000);
    if(!!this.userId){
      this.restApi.restApiWithData("getUserReportByUid", {recentLen:this.getGameLength, dateLen:this.getDateLength, uid:this.userId})
      .then(res=>{
        loading.dismiss();
        if(!!res.result){
          if(res.result.hasOwnProperty('dateGames')) this.reportData = res.result.dateGames;
          if(res.result.hasOwnProperty('recentPPRs')) this.updateOptions(res.result.recentPPRs.reverse());
        }
      })
      .catch(err=>{
        loading.dismiss();
        console.error("ERROR !! : " + err);
      });
    }
    return userReport;
  }

  closeDetailReport(data){
    console.log("close detail show");
    this.detailGame = null;
    this.openDetailReport = false;
  }

  calToGo(base:number, score:Array<number>){
    var _arr = [];
    var _lastScore = base;
    score.forEach(s=>{
      _lastScore -= s;
      _arr.push(_lastScore);
    });
    return _arr;
  }

  async detailReport(data){
    console.log("detail show : " + JSON.stringify(data.game));
    var _detailGame = {};
    var keys = Object.keys(data.game);
    keys.forEach(k => {
      _detailGame[k] = data.game[k];
    });

    var user1Darts = [];
    _detailGame['roundScores'].forEach(v => {
      user1Darts.push(v);
    });

    var user2Darts = [];
    _detailGame['enm_roundScores'].forEach(v=>{
      user2Darts.push(v);
    });

    if(user1Darts.length > user2Darts.length){
      user2Darts.push(0);
    } 
    if(user1Darts.length < user2Darts.length) {
      user1Darts.push(0);
    }
    _detailGame['roundScores'] = user1Darts;
    _detailGame['enm_roundScores'] = user2Darts;

    _detailGame["throwIndex"] = user1Darts.length;
    var _name1 = await this.restApi.restApiWithData("getNameByUid", {uid:_detailGame['uid']});
    _detailGame["userName"] = _name1.result;
    if(String(_detailGame['enm_uid']).length){
      var _name2 = await this.restApi.restApiWithData("getNameByUid", {uid:_detailGame['enm_uid']});
      _detailGame["enmName"] = _name2.result;
    } else {
      _detailGame["enmName"] = "???";
    }
    
    _detailGame["toGo"] = this.calToGo(_detailGame["zeroOne"], _detailGame['roundScores']);
    _detailGame["enmToGo"] = this.calToGo(_detailGame["zeroOne"], _detailGame['enm_roundScores']);
    console.log("detail show : " + JSON.stringify(_detailGame));
    this.detailGame = _detailGame;
    this.openDetailReport = true;
  }

  clickCancel(){
    console.log("report out");
    this.closeCallback.emit({result:null});
  }

  clickOk(){
    console.log("report out");
    this.closeCallback.emit({result:null});
  }
}
