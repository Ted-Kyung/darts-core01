import { Component, EventEmitter, Input, Output } from '@angular/core';

import {User} from '../../model/user';

import { AlertController } from 'ionic-angular';


/**
 * Generated class for the KeyBoardComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'key-board',
  templateUrl: 'key-board.html'
})
export class KeyBoardComponent {

  keyList = ['1', '2', '3', '4', '5', '6', '7', '8', '9', 'DEL', '0', 'OK'];

  _score = 0;

  arrangeScore = false;

  @Input() drData:any;
  @Output() resultCb = new EventEmitter();
  @Output() modifyCb = new EventEmitter();

  constructor(public alertCtrl:AlertController) {
    
  }

  keyboardClick(value){
    console.log("key click : " + value);
    if(Number.isInteger(value)){
      var v = (this._score * 10) + value;
      if(v <= 180) this._score = v;
    }
  }

  keyboardExtraClick(value){
    console.log("Extra key click : " + value);

    if(Number.isInteger(value)){
      this._score = 0;
      this.resultCb.emit(value);
    }
    else {
      if(value === "DEL"){
        var s = String(this._score);
        s = s.slice(0, s.length -1);
        this._score = Number(s);
      } else if (value == "OK") {
        this.resultCb.emit(this._score);
        this._score = 0;
      } else if (value == "BUST") {
        this.resultCb.emit(this._score);
      } else if (value == "FINISH"){
        console.log("finished!! set value : " + this.drData['currentUser'].currentScore);
        this.resultCb.emit(this.drData['currentUser'].currentScore);
      }
    }
  }

  historyClick(index, id){
    var user:User = null;
    if(!id){
      user = this.drData['user1'];
    }
    else {
      user = this.drData['user2'];
    }

    let alert = this.alertCtrl.create({
      title: user.roundScores[index] + ' to ?',
      inputs: [{
        name: 'score',
        type: 'number',
        placeholder: String(user.roundScores[index])
      }],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'OK',
          handler: data => {
            this.modifyCb.emit({'identifier':id, 'index':index, 'score':data.score});
          }
        }
      ]
    });
    alert.present();
  }

  setBackground(){
    if(!!!this.drData) return;
    if(!this.drData['color']){
      return {'background-color': '#397bff'};
    } else {
      return {'background-color': '#cb1744'};
    }
  }
  setHistoryPannel(id){
    var leftHistory = document.getElementsByClassName("histories")[0];
    var rightHistory = document.getElementsByClassName("histories")[1];
    leftHistory.scrollTo(0, leftHistory.scrollHeight);
    rightHistory.scrollTo(0, rightHistory.scrollHeight);
    if(!!!this.drData) return;
    if(id === this.drData['color']){
      if(id === 0){
        return {"background-color": "#397bff"};
      } else {
        return {"background-color": "#cb1744"};
      }
    }
    return {"background-color": "#848484"}
  }
}
