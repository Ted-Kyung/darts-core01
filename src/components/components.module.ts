import { NgModule } from '@angular/core';
import { GameModeComponent } from './game-mode/game-mode';
import { UserScoreComponent } from './user-score/user-score';
import { GameSetupComponent } from './game-setup/game-setup';
import { KeyBoardComponent } from './key-board/key-board';
import { IonicModule, } from 'ionic-angular';
import { FinishGameComponent } from './finish-game/finish-game';
import { CreateAccountComponent } from './create-account/create-account';
import { FindUserComponent } from './find-user/find-user';
import { UserSummaryViewComponent } from './user-summary-view/user-summary-view';
import { FinishCheckComponent } from './finish-check/finish-check';
import { CustomDialogComponent } from './custom-dialog/custom-dialog';
import { UserReportComponent } from './user-report/user-report';
import { LoginPasswordComponent } from './login-password/login-password';

// plugin graph
import { ChartsModule } from 'ng2-charts';
import { NgxEchartsModule } from 'ngx-echarts';
import { DetailReportComponent } from './detail-report/detail-report';
import { NoticeBoardComponent } from './notice-board/notice-board';
@NgModule({
	declarations: [GameModeComponent,
    UserScoreComponent,
    GameSetupComponent,
    KeyBoardComponent,
    FinishGameComponent,
    CreateAccountComponent,
    FindUserComponent,
    UserSummaryViewComponent,
    FinishCheckComponent,
    CustomDialogComponent,
    UserReportComponent,
    LoginPasswordComponent,
    DetailReportComponent,
    NoticeBoardComponent
    ],
	imports: [IonicModule, ChartsModule, NgxEchartsModule],
	exports: [GameModeComponent,
    UserScoreComponent,
    GameSetupComponent,
    KeyBoardComponent,
    FinishGameComponent,
    CreateAccountComponent,
    FindUserComponent,
    UserSummaryViewComponent,
    FinishCheckComponent,
    CustomDialogComponent,
    UserReportComponent,
    LoginPasswordComponent,
    DetailReportComponent,
    NoticeBoardComponent,
    ]
})
export class ComponentsModule {}
