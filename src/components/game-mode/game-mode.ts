import { Component, EventEmitter, Output } from '@angular/core';

/**
 * Generated class for the GameModeComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'game-mode',
  templateUrl: 'game-mode.html'
})
export class GameModeComponent {

  // @Input() matchInfo: object;
  @Output() resultCb = new EventEmitter();

  constructor() {
  }

  public setZeroOne(value:number){
    console.log("click " + value);
    this.resultCb.emit(value);
  }
}
