import { Component, EventEmitter, Input, Output, ViewChild, ElementRef } from '@angular/core';
import { FirebaseRestApiServices } from "../../services/fbRestApi";
import {AlertController, LoadingController, Content} from 'ionic-angular';

// local storage
import { LocalStorageService } from '../../services/lcStorageService';

/**
 * Generated class for the NoticeBoardComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'notice-board',
  templateUrl: 'notice-board.html'
})
export class NoticeBoardComponent {
  @Input() user;
  @Output() closeCb = new EventEmitter();
  @ViewChild('notice') noticeString;
  @ViewChild('middleTop') middleTop: ElementRef;
  @ViewChild(Content) content: Content;

  wordLength = 0;

  openFindUser = false;
  openUserReport = false;

  reportUserUid = "";
  findUserType = "findUser1";

  noticeObjects = {};
  noticeArrayKeys = [];
  noticeLastOlderToken = null;
  noticeArray = [];

  NOTICE_USER = "NOTICE_USER";

  continueNoticeCut = false;
  refrestCut = false;

  noticeStringNg = "";

  infiniteEnable = false;

  constructor(private restApi:FirebaseRestApiServices,
    public storage:LocalStorageService,
    public loadingCtrl:LoadingController,
    public alertCtrl: AlertController,) {
    this.firstLoad();
  }

  ngOnInit() {
    this.content.resize();
  }

  foundUserItem(data){
    this.user = data.result;
    this.storage.setLocalStorage(this.NOTICE_USER, this.user);
    this.openFindUser = false;
    this.sendClick();
  }

  async sendClick(){
    this.user = await this.storage.getLocalStorage(this.NOTICE_USER);
    if(!!!this.user){
      let _alert = this.alertCtrl.create({
        title:"Login",
        subTitle: "Required login",
        buttons:[{
          text: "Cancel",
          handler: ()=>{
            this.cancelFindUser(null);
          }
        },{
          text: "Login",
          handler: ()=>{
            this.openFindUser = true;
          }
        }]
      });
      _alert.present();
    } else {
      if(this.continueNoticeCut){
        let _alert = this.alertCtrl.create({
          title:"Caution",
          subTitle: "Too fast, Try after few minute",
          buttons:[{
            text: "Ok",
          }]
        });
        _alert.present();
      } else {
        let _alert = this.alertCtrl.create({
          title:"Name : " + this.user.name,
          subTitle: "Do you want upload this name?",
          buttons:[{
            text: "Cancel",
            handler: ()=>{
              this.user = null;
              this.storage.removeLocalStorage(this.NOTICE_USER);
              this.openFindUser = true;
            }
          },{
            text: "Yes",
            handler: ()=>{
              let data = {
                name: this.user.name,
                uid: this.user.uid,
                notice: this.noticeStringNg
              }
              console.log("NNNN notice : " + JSON.stringify(data));
              this.restApi.restApiWithData("updateOneLineNoticeBoard", data);
              this.noticeStringNg = "";
              this.continueNoticeCut = true;
              setTimeout(()=>{this.continueNoticeCut = false;}, 60000);
            }
          }]
        });
        _alert.present();
      }
    }
  }

  public hasNextNotice() {
    return this.infiniteEnable;
  }

  private checkScroll(infiniteScroll) {
    if (this.hasNextNotice()) {
      let contentHeight: any = window.screen.height,
          noticesHeight = this.middleTop.nativeElement.offsetHeight;

      console.log("CONTENT HEIGHT", contentHeight);
      console.log("NOTICES HEIGHT", noticesHeight);

      if (parseInt(contentHeight) > parseInt(noticesHeight)) {
        this.loadDataOlder(infiniteScroll);
      } else {
        // Nothing
        infiniteScroll.complete();
      }
    }
  }

  async firstLoad(){
    if(this.refrestCut){
      let ld = this.loadingCtrl.create({
        spinner: 'crescent',
        duration: 200
      });
      ld.present();
      return true;
    } else {
      let ld = this.loadingCtrl.create({
        spinner: 'crescent',
        duration: 10000
      });
      ld.present();
      this.infiniteEnable = false;
      this.noticeArray = [];
      let notices:any = await this.restApi.restApiWithData("getOneLineNewerNoticeBoard", {token:null});
      this.infiniteEnable = true;
      ld.dismiss();
      this.refrestCut = true;
      setTimeout(()=>{this.refrestCut = false;}, 30000);
      let noticeRaw = notices.result;
      let keys = Object.keys(noticeRaw).reverse();
      this.noticeLastOlderToken = keys[0];
      if(keys.length>0){
        await keys.forEach(k=>{
          this.noticeArray.push(noticeRaw[k]);
          this.noticeLastOlderToken = k;
        });
      }
      return true;
    }
  }

  public async loadDataOlder(infiniteScroll) {
    console.log("Load more.", infiniteScroll);
    this.infiniteEnable = false;
    let ld = this.loadingCtrl.create({
      spinner: 'crescent',
      duration: 200
    });
    ld.present();
    let notices:any = await this.restApi.restApiWithData("getOneLineOlderNoticeBoard", {token:this.noticeLastOlderToken});
    ld.dismiss();
    this.infiniteEnable = true;
    let noticeRaw = notices.result;
    let keys = Object.keys(noticeRaw)
    keys.pop();
    console.log("NNNN keys : " + JSON.stringify(keys));
    console.log("NNNN pop keys : " + JSON.stringify(keys));
    if(keys.length>0){
      let _k = keys.reverse();
      await _k.forEach(k=>{
        this.noticeArray.push(noticeRaw[k]);
        this.noticeLastOlderToken = k;
      });
    }
    this.infiniteEnable = true;
    infiniteScroll.complete();
    return true;
  }

  reportShowDone(data){
    this.openUserReport = false;
    this.reportUserUid = "";
  }

  noticeClick(uid){
    this.reportUserUid = uid;
    this.openUserReport = true;
  }

  cancelFindUser(data){
    console.log("cancel found user");
    this.openFindUser = false;
  }

  onChange(){
    var word:string = this.noticeStringNg;
    console.log("NNN word : " + word);
    if(word.length >= 50){
      this.noticeStringNg = word.substr(0, 50);
    }
    console.log("NNN word ng : " + this.noticeStringNg);
    this.wordLength = this.noticeStringNg.length;
  }

  close(){
    console.log("close click!!!!!");
    this.closeCb.emit({});
  }
}
