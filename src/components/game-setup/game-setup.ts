import { Component, EventEmitter, Output } from '@angular/core';

/**
 * Generated class for the GameSetupComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'game-setup',
  templateUrl: 'game-setup.html'
})
export class GameSetupComponent {

  // @Input() matchInfo: object;
  @Output() resultCb = new EventEmitter();

  round = 3;
  constructor() {
  }

  roundControl(type:string){
    console.log("round control type : " + type);
    if(type==='+'){
      this.round++;
    } else {
      if(this.round > 1)this.round--;
    }
  }

}
