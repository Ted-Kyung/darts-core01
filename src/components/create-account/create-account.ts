import { Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';

// using event api
import { LoadingController,AlertController } from 'ionic-angular';

// fb auth service
import { FirebaseLoginService } from '../../services/fbLoginService';

/**
 * Generated class for the CreateAccountComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'create-account',
  templateUrl: 'create-account.html'
})
export class CreateAccountComponent {
  @ViewChild('email') email;
  @ViewChild('passwd') passwd;
  @ViewChild('repasswd') repasswd;
  @ViewChild('name') name;

  _registerAccount = false;
  reason = "";

  @Output() resultCb = new EventEmitter();

  constructor(public fbAuth:FirebaseLoginService, 
              public ldCtrl: LoadingController,
              public altCtrl: AlertController) {

  }

  clickCancel(){
    console.log("click close");
    this.resultCb.emit({"res": null});
  }

  clickOK(){
    this.reason = "";
    console.log("click ok");
    if(this.email.value.length == 0){
      this.reason = "Invalid Email";
    } else if(this.passwd.value.length == 0){
      this.reason = "Invalid password";
    } else if(this.repasswd.value.length == 0){
      this.reason = "Invalid re-password";
    } else if(this.name.value.length == 0){
      this.reason = "Invalid name";
    } else if(this.passwd.value != this.repasswd.value){
      this.reason = "Password and Re-Password not same";
    } else {
      let ld = this.ldCtrl.create({
        spinner: 'crescent',
      });
      ld.present();
      this.fbAuth.createAccountWithEmail(this.email.value, this.passwd.value, this.name.value)
      .then((result)=>{
        ld.dismiss();
        console.log("Success create account : " + JSON.stringify(result));
        this.resultCb.emit({"res": {"email": result.email,
                            "uid": result.uid,
                            "name": this.name.value}});
      })
      .catch((error)=>{
        ld.dismiss();
        let al = this.altCtrl.create({
          title:"Error",
          subTitle: error
        });
        al.present();
      });
    }
  }
}
