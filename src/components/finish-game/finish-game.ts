import { Component, EventEmitter, Input, Output } from '@angular/core';
import { User } from "../../model/user";

/**
 * Generated class for the FinishGameComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'finish-game',
  templateUrl: 'finish-game.html'
})
export class FinishGameComponent {
  @Input() user1: User;
  @Input() user2: User;
  @Output() nextGameCb = new EventEmitter();
  @Output() resetOfGameCb = new EventEmitter();

  constructor() {
  }

  clickSaveButton(){
    console.log("clickSaveButton");
  }

  clickNextGame(){
    console.log("clickNextGame");
    this.nextGameCb.emit(null);
  }
  clickResetGame(){
    console.log("clickResetGame");
    this.resetOfGameCb.emit(null);
  }
}
