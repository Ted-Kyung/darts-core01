import { Component, EventEmitter, Input, Output } from '@angular/core';

/**
 * Generated class for the DetailReportComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'detail-report',
  templateUrl: 'detail-report.html'
})
export class DetailReportComponent {
  @Input() game:string;
  @Output() closeCallback = new EventEmitter();

  startDarts = 3;


  constructor() {
  }

  outSideClick(){
    this.closeCallback.emit({});
  }

  getScoreStyle(score){
    if(score > 140){
      return {
        'background-color': "#ffa5a7"
      }
    }
    if(score > 100){
      return {
        'background-color': "#ffde6a"
      }
    }
  }

  getToGoScoreStyle(score){
    if(score === 0){
      return {
        'background-color': "#a7ffa7"
      }
    }
  }
}
