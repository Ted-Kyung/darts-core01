import { Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';

// default modules 
import { LoadingController,AlertController } from 'ionic-angular'
// rest api services
import { FirebaseRestApiServices } from "../../services/fbRestApi";

/**
 * Generated class for the FindUserComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'find-user',
  templateUrl: 'find-user.html'
})
export class FindUserComponent {
  @ViewChild('name') name;

  @Input() type: string;
  @Output() chooseUser = new EventEmitter();
  @Output() cancel = new EventEmitter();

  findResult = [];
  selectedIndex = -1;
  selectedUserItem = null;

  // needLoginEmail
  opneLoginDialog = false;

  loginUserItem = null;

  constructor(private fbRest:FirebaseRestApiServices,
              private loadingCtrl: LoadingController) {

  }

  ////// STYLE 
  getUserItemStyle(i){
    if(i === this.selectedIndex){
      return {
        'color': '#CA4545',
        'font-size': '1.45em'
      };
    } else {
      return {
        'color': '#454545',
        'font-size': '1.3em'
      }; 
    }
  }
  getButtonStyle(){
    if(-1 === this.selectedIndex){
      return {
        'background-color': '#454545',
      };
    }
  }
  ////// STYLE 

  private _showProgress(timeout:number){
    let loading = this.loadingCtrl.create({
      spinner: 'crescent',
      duration: timeout
    });
    loading.present();
    return loading;
  }

  private _stopProgress(progress: any){
    progress.dismiss();
  }

  private async _findUserByName(name: string){
    try {
      console.log("_findUserByName : " + name);
      return await this.fbRest.restApiWithData("findUserByName", {"name": name});
    }
    catch {
      return null;
    }
  }

  private async _findUserResultByName(name: string){
    try {
      console.log("_findUserResultByName : " + name);
      return await this.fbRest.restApiWithData("findUserResultByName", {"name": name});
    }
    catch {
      return null;
    }
  }

  loginResultHandler(data){
    console.log("login data : " + JSON.stringify(data));
    this.opneLoginDialog = false;
    if(data.result){
      console.log("clickOK: " + JSON.stringify(this.findResult[this.selectedIndex]));
      this.chooseUser.emit({type: this.type, result: this.findResult[this.selectedIndex]});
    }
  }

  clickedUserItem(i, name:string){
    console.log("clicked " + name + " !!!! ")
    if(this.selectedIndex === i){ 
      this.selectedUserItem = null;
      this.selectedIndex = -1; 
    }
    else { 
      this.selectedUserItem = this.findResult[i];
      console.log("selected Item : " + JSON.stringify(this.selectedUserItem));
      this.selectedIndex = i; 
    }
  }

  public async findUser(){
    this.findResult = [];
    let _loading = this._showProgress(10000);
    var _name:string = this.name.value;
    if(_name.length){
      console.log("findUser : " + _name);
      var _res = null;
      // reason : regist user from sidebar
      if((this.type === "findUser1")||(this.type == "findUser2")){
        _res = await this._findUserByName(_name);
      }
      // reason : show user report 
      if(this.type === "findUserReport") {
        _res = await this._findUserResultByName(_name);
      }
      console.log("_res: " + JSON.stringify(_res));
      this._stopProgress(_loading);
      this.findResult = _res.result;
      console.log("find _res.data.result : "+ JSON.stringify(this.findResult));
    } else {
      this._stopProgress(_loading);
      console.log("length 0");
    }
  }

  clickOK(){
    if(this.selectedIndex !== -1){
      if(this.type !== "findUserReport"){
        this.opneLoginDialog = true;
      } else {
        this.chooseUser.emit({type: this.type, result: this.findResult[this.selectedIndex]})
      }
    }
  }

  clickCancel(){
    console.log("clickCancel");
    this.cancel.emit({result: null});
  }
}
