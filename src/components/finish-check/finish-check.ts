import { Component, EventEmitter, Input, Output } from '@angular/core';

/**
 * Generated class for the FinishCheckComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'finish-check',
  templateUrl: 'finish-check.html'
})
export class FinishCheckComponent {

  choiceNumber = 0;
  @Output() resultCb = new EventEmitter();

  constructor() {
  }

  getBoxStyle(number){
    if(number == this.choiceNumber){
      return {
        'background-color': '#cb2855',
        'color': '#FFFFFF'
      };
    } else {
      return {
        'background-color': '#8f8f8f',
        'color': '#454545'
      };
    }
  }

  getButtonStyle(){
    if(this.choiceNumber){
      return {
        'background-color': '#5A8bff',
        'color': '#FFFFFF'
      };
    } else {
      return {
        'background-color': '#cfcfCf',
        'color': '#454545'
      };
    }
  }

  clickDarts(number){
    if(this.choiceNumber != number) this.choiceNumber = number;
    else {
      this.choiceNumber = 0;
    } 
  }

  clickCancel(){
    console.log("cleck Cancel");
    this.choiceNumber = 0;
    this.resultCb.emit({result:this.choiceNumber});
  }

  clickOk(){
    if(this.choiceNumber > 0){
      console.log("cleck Ok");
      this.resultCb.emit({result:this.choiceNumber});
    }
  }
}
