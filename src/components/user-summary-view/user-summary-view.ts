import { Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { Events } from 'ionic-angular';

/**
 * Generated class for the UserSummaryViewComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'user-summary-view',
  templateUrl: 'user-summary-view.html'
})
export class UserSummaryViewComponent {
  @Input() userData;
  @Input() userIdent;

  constructor(private event: Events) {
  }

  userSummaryClick(){
    // TODO : Show user History page 
    console.log("userSummaryClick : " + JSON.stringify(this.userData));
    this.event.publish(this.userIdent+"SummaryClick", {result:this.userData});
  }
  userChangeClick(){
    // TODO : Do User Change 
    console.log("userChangeClick : " + JSON.stringify(this.userData));
    this.event.publish(this.userIdent+"ChangeClick", {result:this.userData});
  }
}
