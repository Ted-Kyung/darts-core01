import { Component, EventEmitter, Input, Output } from '@angular/core';
import { User } from "../../model/user";

/**
 * Generated class for the UserScoreComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'user-score',
  templateUrl: 'user-score.html'
})
export class UserScoreComponent {

  @Input() userInfo: User;
  @Output() resultCb = new EventEmitter();
  @Output() userChangeCb = new EventEmitter();

  // show params
  currentScore = 0;
  constructor() {
    this._active();
  }
  scoreClick(){
    this.resultCb.emit(this.userInfo);
  }

  userClick(){
    this.userChangeCb.emit(this.userInfo.identifier);
  }

  _active(){
    if(!!this.userInfo){
      this.currentScore = this.userInfo.currentScore;
    }
  }
}
