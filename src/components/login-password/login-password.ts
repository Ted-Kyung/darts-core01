import { Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';

import {FirebaseLoginService} from '../../services/fbLoginService';

import { LoadingController } from 'ionic-angular';

@Component({
  selector: 'login-password',
  templateUrl: 'login-password.html'
})
export class LoginPasswordComponent {
  @ViewChild('passwd') passwd;
  @Input() userItem:any;
  @Output() resultCb = new EventEmitter();
  constructor(
    public fbLogin:FirebaseLoginService,
    public loadingCtrl:LoadingController) {
  }

  clickCancel(){
    console.log("cleck Cancel");
    this.resultCb.emit({result: false});
  }

  clickOk(){
    console.log("cleck Ok : passwd: " + this.passwd.value + ", user Item : " + JSON.stringify(this.userItem));
    this.fbLogin.loginWithEmail(this.userItem.email, this.passwd.value)
    .then(()=>{
      this.resultCb.emit({result: true});
    })
    .catch((err)=>{
      console.log("error :" + err);
    });
  }
}
