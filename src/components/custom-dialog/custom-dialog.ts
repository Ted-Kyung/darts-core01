import { Component, EventEmitter, Input, Output } from '@angular/core';

/**
 * Generated class for the CustomDialogComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'custom-dialog',
  templateUrl: 'custom-dialog.html'
})
export class CustomDialogComponent {

  @Input() topic:string;
  @Input() titleText:string;
  @Input() textBody:string;
  @Input() btnOnlyOk:boolean;

  @Output() resultCb = new EventEmitter();

  constructor() {
  }



  clickCancel(){
    console.log("cleck Cancel");
    this.resultCb.emit({topic: this.topic, result:false});
  }

  clickOk(){
    console.log("cleck Ok");
    this.resultCb.emit({topic: this.topic, result:true});
  }
}
