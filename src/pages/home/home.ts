import { Component } from '@angular/core';
import { NavController, Events, MenuController, AlertController, Platform } from 'ionic-angular';

// services import
import { FirebaseRestApiServices } from '../../services/fbRestApi';

// local storage
import { LocalStorageService } from '../../services/lcStorageService';

// plugin
import { AppVersion } from '@ionic-native/app-version';

// model
import { User } from '../../model/user';

// jquery
import * as $ from 'jquery'

// constant
const STORAGE_USER1 = "USER1";
const STORAGE_USER2 = "USER2";
const EVENT_FINDUSER1_RESULT = "findUser1Result";
const EVENT_FINDUSER2_RESULT = "findUser2Result";
const EVENT_UPDATE_USER_DATA = "userDataUpade";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  // directive handler
  isReady = false;
  isOpenMenu = false;

  // page contorller
  openModeSelect = false;
  openGameSetup = false;
  openKeyBoard = false;
  openFinishedGame = false;
  openFinishedCheck = false;
  openCreateAccount = false;
  openFindUser = false;
  openCustomDialog = false;
  openUserReport = false;
  opneLoginDialog = false;
  openNoticeBoard = false;

  // user model
  user1 = new User(0);
  user2 = new User(1);

  // current user
  currentUserIdent = 0;
  currentUser:User;

  // diddle user
  diddleUser = -1;

  // find user type
  findUserType = "";
  regitUserDatas = null;

  // UI
  isArrange = false;
  keyboardData = {};
  user1FinishedData = [];
  user2FinishedData = [];

  //version
  _publicVersion = "";
  _currentVersion = "";

  // getReport
  reportUserUid = "";

  // custom dialog
  cTopic = "";
  cTitleText = "";
  cTextBody = "";
  cOnlyOk = false;

  // online Notice User 
  noticeLoginUser: User;


  constructor(public navCtrl: NavController,
              public events: Events,
              public menuCtrl: MenuController,
              public restApi: FirebaseRestApiServices,
              public alertCtrl: AlertController,
              public version: AppVersion,
              public platform: Platform,
              public localStorage: LocalStorageService) {
    
    platform.ready().then(()=>{
      this._active();
    });
  }

  // openModeSelect Handler
  setMode(data){
    console.log("setMode received data : " + data);
    this.menuCtrl.enable(true);
    this.menuCtrl.swipeEnable(true);
    // close set mode directive
    this.openModeSelect = false;
    // set zero one base
    this.user1.zeroOne = data;
    this.user2.zeroOne = data;
    this.user1.startMatch(this.user1.currentWinCount + this.user2.currentWinCount);
    this.user2.startMatch(this.user1.currentWinCount + this.user2.currentWinCount);
    this._changeCurrentUser();
  }

  // .bk-accent-blue{
  //   background-color: #397bff;
  // }
  // .bk-accent-red{
  //   background-color: #cb1744;
  // }
  userScoreBackGround(id){
    if(id === this.currentUserIdent){
      if(id === 0){
        return {"background-color": "#397bff"};
      } else {
        return {"background-color": "#cb1744"};
      }
    }
    return {"background-color": "#848484"}
  }

  arrowStyle(){
    if(this.isOpenMenu){
      return {"transform": "rotate(180deg)"};
    };
    return {"transform": "rotate(0deg)"};
  }

  // userScore Handler
  clickUserScore(data){
    console.log("clickUserScore received data : " + data);
    let _user:User = data;
    this.currentUserIdent = (_user.identifier + 1) % 2; 
    this._changeCurrentUser();
  }
  
  // click user change in user-score page handler
  clickUserChange(data){
    $('.menu-btn').click();
  }

  // gameSetting Handler
  gameSetup(data){
    console.log("clickGameSetup received data : " + data);
    this.openGameSetup = false;
  }

  // create User (register user) Handler
  registAccountResult(data){
    this.menuCtrl.enable(true);
    this.menuCtrl.swipeEnable(true);
    if(!!data.res){
      let _email = data.res.email;
      let _uid = data.res.uid;
      let _name = data.res.name;
      let _restData = {
        "email": _email,
        "uid": _uid,
        "name": _name
      }
      console.log("success created account : " + _email + ", " + _uid + ", " + _name);
      this.restApi.restApiWithData("createNewUser", _restData)
      .then(data=>{
        console.log("success regist database"+ data);
        this.openCreateAccount = false;
      })
      .catch(err=>{
        console.log("failed regist database" + err);
      });
    } else {
      this.openCreateAccount = false;
    }
  }

  // handle for finished darts count checker
  async finishedDartsResult(data){
    this.menuCtrl.enable(true);
    this.menuCtrl.swipeEnable(true);
    this.openFinishedCheck = false;
    if(data.result>0){
      // finished game, do show game-ended page
      console.log("Finished Game!!!");
      this.user1.brushUpResult(data, this.user2.roundScores);
      this.user2.brushUpResult(data, this.user1.roundScores);
      this.user1FinishedData = this.user1.resultData;
      this.user2FinishedData = this.user2.resultData;
      this.openFinishedGame = true;
      // TODO : Save Server , if regist user 
      await this._uploadGameResultToServer();
      let _uids = {
        "user1": this.user1,
        "user2": this.user2,
      } 
      this.events.publish(EVENT_UPDATE_USER_DATA, _uids);
      console.log("goToNextGame : " + data);
    }
    else {
      this.currentUser.scoreRollback();
    }
  }

  private _checkFinishedDartsCount(){
    // open finished darts check page
    this.menuCtrl.enable(false);
    this.menuCtrl.swipeEnable(false);
    this.openFinishedCheck = true;
  }

  handleNoticeBoard(data){
    console.log("close notice board");
    this.openNoticeBoard = false;
  }

  // input score Handler
  inputScore(data){
    console.log("clickInputScore received data : " + data);
    this.currentUser.setScore(data).then(()=>{
      // if first score, set to diddle user 
      if(this.diddleUser == -1) this.diddleUser = this.currentUser.identifier;
      // not finish, change current user
      this._changeCurrentUser();
    }).catch(async ()=>{
      // show popup many darts count
      this._checkFinishedDartsCount();
    });
  }

  // modify score from History directive Handler
  modifyScore(modifyInfo){
    console.log("clickInputScore received ident : " + modifyInfo.identifier + " index : " + modifyInfo.index + " score : " + modifyInfo.score);
    if(!modifyInfo.identifier){
      this.user1.modifyScoreWithIndex(modifyInfo.index, modifyInfo.score);
      if(this.user1.currentScore === 0) this._checkFinishedDartsCount();
    } else {
      this.user2.modifyScoreWithIndex(modifyInfo.index, modifyInfo.score);
      if(this.user2.currentScore === 0) this._checkFinishedDartsCount();
    }
  }

  private async _uploadGameResultToServer(){
    if(this.user1.uid.length){
      console.log("user1 game data send to server");
      this.restApi.restApiWithData("gameDataWithUid", this.user1);
    }
    if(this.user2.uid.length){
      console.log("user2 game data send to server");
      this.restApi.restApiWithData("gameDataWithUid", this.user2);
    }
  }

  // click next game from finishGame Handler
  async goToNextGame(data){
    let matchId = this.user1.currentWinCount + this.user2.currentWinCount;
    this.user1.startMatch(matchId);
    this.user2.startMatch(matchId);
    // next game must base diddle, not loser
    this.currentUserIdent = (matchId+1) % 2;
    this._changeCurrentUser();
    this.openFinishedGame = false;
    let _uids = {
      "user1": this.user1,
      "user2": this.user2,
    } 
    this.events.publish(EVENT_UPDATE_USER_DATA, _uids)
    this.menuCtrl.enable(true);
    this.menuCtrl.swipeEnable(true);
  }

  // click end game from finishGame Handler
  doResetOfGame(data){
    console.log("doResetOfGame : " + data);
    let matchId = this.user1.currentWinCount + this.user2.currentWinCount;
    this.user1.resetAll(matchId);
    this.user2.resetAll(matchId)
    // next game must base diddle, not loser
    this.currentUserIdent = (matchId+1) % 2;
    this._changeCurrentUser();
    this.openFinishedGame = false;
    let _uids = {
      "user1": this.user1,
      "user2": this.user2,
    } 
    this.events.publish(EVENT_UPDATE_USER_DATA, _uids)
    this.menuCtrl.enable(true);
    this.menuCtrl.swipeEnable(true);
  }

  private _checkValidationUser(type:string, ref, result){
    console.log("_checkValidationUser: " + type + ", ref :" + JSON.stringify(ref) + ", result : " + JSON.stringify(result));
    if((!!ref)&&(ref.email === result.email)){
      let _alert = this.alertCtrl.create({
        title:"Caution",
        subTitle: "Can't regist same id : " + ref.email,
        buttons:[{
          text: "OK"
        }]
      });
      _alert.present();
      console.warn("same id !!");
    } else {
      $('.menu-btn').click();
      console.log("event publish : " + type+"Result" + ", user item : " + JSON.stringify(result));
      this.events.publish(type+"Result", result);
    }
  }

  // choose from find user handler, it is must have value
  foundUserItem(res){
    // initialize
    this.menuCtrl.enable(true);
    this.menuCtrl.swipeEnable(true);

    this.openFindUser = false;
    this.findUserType = "";
    
    // after handlling
    console.log("foundUserItem : " + JSON.stringify(res));
    if(res.type === "findUser1"){
      this._checkValidationUser(res.type, this.regitUserDatas.user2, res.result);
    } 
    else if(res.type === "findUser2"){
      this._checkValidationUser(res.type, this.regitUserDatas.user1, res.result);
    } 
    else {
      console.log("found user res : " + JSON.stringify(res));
      console.log("found user item in finduser component : " + res.result.uid);
      this.reportUserUid = res.result.uid;
      this.menuCtrl.enable(false);
      this.menuCtrl.swipeEnable(false);
      this.openUserReport = true;
    }
  }

  // cancel from find user page Handler
  cancelFindUser(no_data){
    console.log("close find user component");
    this.menuCtrl.enable(true);
    this.menuCtrl.swipeEnable(true);

    this.openFindUser = false;
    this.findUserType = "";
  }

  // just change current user
  private _changeCurrentUser(){
    let d = {};
    this.currentUserIdent = (this.currentUserIdent + 1) % 2;
    d['color'] = this.currentUserIdent;
    if(this.currentUserIdent%2 == 0){
      this.currentUser = this.user1;
    }
    else {
      this.currentUser = this.user2;
    }
    d['user1'] = this.user1;
    d['user2'] = this.user2;
    d['history1'] = this.user1.roundScores;
    d['history2'] = this.user2.roundScores;
    d['currentUser'] = this.currentUser;

    if(this.currentUser.currentScore>170) {
      d['isArrange'] = false;
      this.isArrange = false;
    }
    else if(this.currentUser.currentScore<=170){
      d['isArrange'] = true;
      this.isArrange = true;
    }
    this.keyboardData = d;
  }
  
  private _setFindUserType(type:string){
    this.findUserType = type;
    this.menuCtrl.enable(false);
    this.menuCtrl.swipeEnable(false);
    this.openFindUser = true;
  }

  private _eventInitialize(){
    this.events.subscribe("menuOpen", ()=>{
      // just gray background show
      console.log("event menu Open");
      this.isOpenMenu = true;
    });
    this.events.subscribe("menuClose", ()=>{
      // just gray background hide
      console.log("event menu close");
      this.isOpenMenu = false;
    });
    this.events.subscribe("createAccount", ()=>{
      // just gray background hide
      console.log("event menu create Account");
      this.menuCtrl.enable(false);
      this.menuCtrl.swipeEnable(false);
      this.openCreateAccount = true;
    });
    this.events.subscribe("findUser1", (userDatas)=>{
      this.regitUserDatas = userDatas;
      console.log("event findUser1");
      this._setFindUserType("findUser1");
    });
    this.events.subscribe("findUser2", (userDatas)=>{
      this.regitUserDatas = userDatas;
      console.log("event findUser2");
      this._setFindUserType("findUser2");
    });
    this.events.subscribe("findUserReport", ()=>{
      console.log("event findUserReport");
      this._setFindUserType("findUserReport");
    });
    this.events.subscribe("ChangedUser1", (val)=>{
      console.log("event ChangedUser1");
      this.localStorage.setLocalStorage(STORAGE_USER1, val);
      this.user1.setUserItem(val.name, val.email, val.uid);
      this.user2.setEnemyItem(val.uid);
    });
    this.events.subscribe("ChangedUser2", (val)=>{
      console.log("event ChangedUser2");
      this.localStorage.setLocalStorage(STORAGE_USER2, val);
      this.user2.setUserItem(val.name, val.email, val.uid);
      this.user1.setEnemyItem(val.uid);
    });
    this.events.subscribe("changeMode", ()=>{
      this.openModeSelect = true;
      this.menuCtrl.enable(false);
      this.menuCtrl.swipeEnable(false);
      this._changeCurrentUser();
    });
    this.events.subscribe("user1SummaryClick", async (data)=>{
      this.reportUserUid = data.result.uid;
      this.menuCtrl.enable(false);
      this.menuCtrl.swipeEnable(false);
      this.openUserReport = true;
    });
    this.events.subscribe("user2SummaryClick", async (data)=>{
      this.reportUserUid = data.result.uid;
      this.menuCtrl.enable(false);
      this.menuCtrl.swipeEnable(false);
      this.openUserReport = true;
    });
    this.events.subscribe("user1ChangeClick", async (data)=>{
      this._showChangeUserDialog("user1", data.result);
    });
    this.events.subscribe("user2ChangeClick", async (data)=>{
      this._showChangeUserDialog("user2", data.result);
    });
    this.events.subscribe("oneLineNotice", ()=>{
      // TODO : season top player Show
      // this._showNoticeUserLogin();
      this.openNoticeBoard = true;
    });

    // on foreground
    document.addEventListener("resume", () => {
      console.log("NNNNNNN resume!!!!!!!!!");
      this._versionChecker();
    }, false);
  }

  private async _getUserItemFromStorage(){
    let _u1 = await this.localStorage.getLocalStorage(STORAGE_USER1);
    let _u2 = await this.localStorage.getLocalStorage(STORAGE_USER2);

    if(_u1){
      this.user1.setUserItem(_u1.name, _u1.email, _u1.uid);
      this.user2.setEnemyItem(_u1.uid);
      this.events.publish(EVENT_FINDUSER1_RESULT, _u1);
    } else {
      this.user2.removeEnermyUserItem();
    }

    if(_u2){
      this.user2.setUserItem(_u2.name, _u2.email, _u2.uid);
      this.user1.setEnemyItem(_u2.uid);
      this.events.publish(EVENT_FINDUSER2_RESULT, _u2);
    } else {
      this.user1.removeEnermyUserItem();
    }
  }

  public reportShowDone(data){
    this.reportUserUid = null;

    this.menuCtrl.enable(true);
    this.menuCtrl.swipeEnable(true);
    
    this.openUserReport = false;
  }

  private _showChangeUserDialog(topicRef, data){
    this.cTopic = topicRef + "Change";
    this.cTitleText = "User Change";
    this.cTextBody = "Do you want Logout " + data.name + " ?";
    this.cOnlyOk = false;
    this.menuCtrl.enable(false);
    this.menuCtrl.swipeEnable(false);
    this.openCustomDialog = true;
  }

  // private _showSeasonTopPlayer(){
  //   this.cTopic = "SeasonTop";
  //   this.cTitleText = "Season Top Player";
  //   this.cTextBody = "Comming soon.. (2019.03.31)";
  //   this.cOnlyOk = true;
  //   this.menuCtrl.enable(false);
  //   this.menuCtrl.swipeEnable(false);
  //   this.openCustomDialog = true;
  // }

  private _checkVersionNumbers(){
    let _currentVersionArray = this._currentVersion.split('.');
    let _publicVersionArray= this._publicVersion.split('.');
    console.log("cV : " + this._currentVersion + ", cvA : " + _currentVersionArray + ", pV : " + this._publicVersion + ", pvA : " + _publicVersionArray);
    if((_publicVersionArray[0] > _currentVersionArray[0])||
      (_publicVersionArray[1] > _currentVersionArray[1])||
      (_publicVersionArray[2] > _currentVersionArray[2])){
      console.log("do must app upgrade");

      this.cTopic = "Upgrade";
      this.cTitleText = "Upgrade";
      this.cTextBody = "A mandatory upgrade exists. Please upgrade";
      this.cOnlyOk = true;
      this.menuCtrl.enable(false);
      this.menuCtrl.swipeEnable(false);
      this.openCustomDialog = true;
    }
    else {
      console.log("public version");
    }
  }

  private _versionChecker(){
    this.version.getVersionNumber().then(_cV=>{
      this._currentVersion = _cV;
      if(this._publicVersion.length === 0){
        this.restApi.restApiWithData("getPublicVersion", {}).then(_pV => {
          console.log("public source server .. ");
          this._publicVersion = _pV.result;
          this._checkVersionNumbers();
        });
      }
      else{
        console.log("public source local .. ");
        this._checkVersionNumbers();
      }
    });
 }

  public customDialogResult(data){
    console.log("customDialogResult : " + JSON.stringify(data));
    // close dialog
    this.menuCtrl.enable(true);
    this.menuCtrl.swipeEnable(true);
    this.openCustomDialog = false;
    // data.result -> true, false;
    // data.topic -> "string"
    if(data.topic === "Upgrade"){
      // TODO : inapp browser url go, 
      console.log("go to upgrade url");
      window.open("http://onelink.to/kgn6mk", "_system");
    }

    // if(data.topic === "SeasonTopPlayer"){
    //   console.log("Season top player");
    // }

    if(data.topic === "user1Change"){
      console.log("change user result : " + data.result);
      // remove confirm
      if(data.result){
        this.user1.removeUserItem();
        this.user2.removeEnermyUserItem();
        this.localStorage.removeLocalStorage(STORAGE_USER1);
        this.events.publish(EVENT_FINDUSER1_RESULT, null);
      }
    }

    if(data.topic === "user2Change"){
      console.log("change user result : " + data.result);
      // remove confirm
      if(data.result){
        this.user2.removeUserItem();
        this.user1.removeEnermyUserItem();
        this.localStorage.removeLocalStorage(STORAGE_USER2);
        this.events.publish(EVENT_FINDUSER2_RESULT, null);
      }
    }

    // custom dialog initialize
    this.cTopic = "";
    this.cTitleText = "";
    this.cTextBody = "";
    this.cOnlyOk = false;
  }

  private async _active(){
    // check is initialize? 
    this.isReady = true;
    this._versionChecker();
    // load from storage
    this._getUserItemFromStorage();
    // set current user 
    this._changeCurrentUser();
    this.openKeyBoard = true;
    if((this.user1.zeroOne == 0) || (this.user2.zeroOne == 0)){
      this.menuCtrl.enable(false);
      this.menuCtrl.swipeEnable(false);
      // this.openGameSetup = true;  // TODO 
      
      // origin
      this.openModeSelect = true;
      // this.openNoticeBoard = true;
    }
    this._eventInitialize();
  }
}
