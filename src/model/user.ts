import { _ParseAST } from "@angular/compiler";

export class User {
  identifier:number;
  name:string;
  email:string;
  uid: string;

  enm_uid:string;
  enm_roundScores:Array<number>;

  roundScores:Array<number>;
  roundUsedDarts:number;
  roundAveragePPR:number;
  matchAveragePPR:number;
  highstPPR:number;
  lowestPPR:number;
  currentWinCount:number;
  currentLoseCount:number;
  highOut:number;
  outSuccess:number;
  outChance:number;
  outSuccessPercent:number;
  tonOutSuccess:number;
  tonOutChance:number;
  tonOutSuccessPercent:number;
  currentOutSuccess:number;
  currentOutChance:number;
  currentTonOutSuccess:number;
  currentTonOutChance:number;
  zeroOne:number;
  roundTon:number;
  matchTon:number;
  roundTon80:number;
  matchTon80:number;
  currentScore:number;
  outScore:number;
  resultData = [];
  _roundCount = 0;

  _winCount = 0;
  _loseCount = 0;

  constructor(idx:number) {
    this.identifier = idx;
    this.name = "";
    this.email = "";
    this.uid = "";

    this.enm_uid = "";
    this.enm_roundScores = [];
  
    this.roundScores = [];
    this.roundUsedDarts = 0;
    this.roundAveragePPR = 0.0;
    this.matchAveragePPR = 0.0;
    this.highstPPR = 0;
    this.lowestPPR = 0;
    this.currentWinCount = 0;
    this.currentLoseCount = 0;
    this.highOut = 0;
    this.outSuccess = 0;
    this.outChance = 0;
    this.outSuccessPercent = 0.0;
    this.tonOutSuccess = 0;
    this.tonOutChance = 0;
    this.tonOutSuccessPercent = 0.0;
    this.currentOutSuccess = 0;
    this.currentOutChance = 0;
    this.currentTonOutSuccess = 0;
    this.currentTonOutChance = 0;
    this.zeroOne = 0;
    this.roundTon = 0;
    this.matchTon = 0;
    this.roundTon80 = 0;
    this.matchTon80 = 0;
    this.currentScore = 0;
    this.outScore = 0;
    this.resultData = [];

    this._winCount = 0;
    this._loseCount = 0;
  }

  private _calcAvgPPR(){
    var len = this.roundScores.length;
    console.log("_calcAvgPPR, len : " + len);
    if(len){
      var total:number = 0;
      this.roundScores.forEach((s:number, index) => {
        total = Number(total) + Number(s);
        console.log("total : " + total + ", s : " + s + ", len : " + len);
        if(index == len-1){
          this.roundAveragePPR = (total / len);
          this.roundAveragePPR = Number(this.roundAveragePPR.toFixed(2));
        }
      });
    }
  }

  private _checkLowHigScore(value:number){
    if(this.highstPPR < value) this.highstPPR = value;
    if(this.lowestPPR == 0){
      this.lowestPPR = value;
    } else {
      if(this.lowestPPR > value) this.lowestPPR = value;
    }
  }

  public setZeroOne(value:number){
    this.zeroOne = value;
  }

  public clearZoroOne(){
    this.zeroOne = 0;
  }

  public modifyScoreWithIndex(idx:number, score:number){
    this.roundScores[idx] = score;
    this._scoreRefresh();
  }

  public setScore(value:number):Promise<any>{
    return new Promise((resolve, reject)=>{
      this._checkFinishChance();
      // over score check
      if(value > this.currentScore) value = 0;
      // 1 left check
      if((this.currentScore - value) === 1) value = 0;
      // 1. input round history
      this.roundScores.push(value);
      // 2. calc average ppr
      this._calcAvgPPR();
      // 3. check highst & lowest
      this._checkLowHigScore(value);
      // 4. calc current score - value
      this.currentScore = this.currentScore - value;
      // 5. check award
      this._checkAward(value);
      // last . check finish?
      if(this.currentScore == 0){
        // finished
        this.outScore = value;
        // finish score update 
        this._outScore(value);
        reject();
      }
      else{
        // keep going
        resolve();
      }
    });
  }

  public async _scoreRefresh(){
    console.log("_score Refresh");
    this.roundUsedDarts = 0;
    this.roundAveragePPR = 0.0;
    this.highstPPR = 0;
    this.lowestPPR = 0;
    this.currentOutSuccess = 0;
    this.currentOutChance = 0;
    this.currentTonOutSuccess = 0;
    this.currentTonOutChance = 0;
    this.roundTon = 0;
    this.roundTon80 = 0;
    this.currentScore = this.zeroOne;
    this.outScore = 0;
    this.resultData = [];
    this._winCount = 0;
    this._loseCount = 0;
    // 1. calc average ppr
    this._calcAvgPPR();
    // 2. clac low and high score
    await this.roundScores.forEach(async (value:number, index) => {
      this._checkFinishChance();
      if(value > this.currentScore) value = 0;
      this._checkLowHigScore(value);
      this._checkAward(value);
      this.currentScore -= value;
      if(this.currentScore == 0){
        this.outScore = value;
        this._outScore(value);
      }
    });
  }

  public _outScore(value:number){
    if(this.highOut < value) this.highOut = value;
  }

  public _checkAward(value:number){
    if(value == 180){
      this.roundTon80++;
      this.matchTon80++;
    } else if(value >= 100){
      this.roundTon++;
      this.matchTon++;
    }
  }

  public _checkFinishChance(){
    if(this.currentScore < 100){
      this.currentOutChance++;
    } 
    if((this.currentScore <= 170) && (this.currentScore>=100)){
      this.currentTonOutChance++;
    }

  }

  public _calcFinishPercentage(){
    this.outSuccess += this.currentOutSuccess;
    this.outChance += this.currentOutChance;
    this.tonOutSuccess += this.currentTonOutSuccess;
    this.tonOutChance += this.currentTonOutChance;

    if(this.outChance>0){
      console.log(this.outSuccess + "," + this.outChance + "," + this.outSuccessPercent);
      this.outSuccessPercent = Number(((this.outSuccess/this.outChance)*100).toFixed(2));
    }
    if(this.tonOutChance>0){
      console.log(this.tonOutSuccess + "," + this.tonOutChance + "," + this.tonOutSuccessPercent);
      this.tonOutSuccessPercent = Number(((this.tonOutSuccess/this.tonOutChance)*100).toFixed(2));
    }
  }

  brushUpResult(data, enmData:Array<number>){
    // check out score
    enmData.forEach(v=>{
      this.enm_roundScores.push(v);
    });
    if(this.outScore == 0){
      // loser
      this.currentLoseCount++;
      this._loseCount = 1;
      this._winCount = 0;
      this.roundUsedDarts = this.roundScores.length * 3;
    } else {
      this.currentWinCount++;
      this._winCount = 1;
      this._loseCount = 0;
      this.roundUsedDarts = (this.roundScores.length-1 * 3) + data;
      if(this.outScore >= 100){
        this.currentTonOutSuccess++;
      } else {
        this.currentOutSuccess++;
      }
    }

    //used darts count
    
    // calculate Finish Percent
    this._calcFinishPercentage();
    // count up round count
    this._roundCount++;
    // add Average ppr for match ppr
    this.matchAveragePPR = Number(this.matchAveragePPR) + Number(this.roundAveragePPR);
    // result data intergrated for result page 
    this.resultData = [
      ["Win", this.currentWinCount],
      ["Round Avg PPR", this.roundAveragePPR.toFixed(2)],
      ["Match Avg PPR", (this.matchAveragePPR / this._roundCount).toFixed(2)],
      ["High Out", this.highOut],
      ["TON", this.matchTon + "(+" + this.roundTon + ")"],
      ["TON 80", this.matchTon80 + "(+" + this.roundTon80 + ")"],
      ["Highst PPR", this.highstPPR],
      ["Lowest PPR", this.lowestPPR],
      ["Out (2 - 99)", this.outSuccessPercent + "%"],
      ["Out (100 - 170)", this.tonOutSuccessPercent + "%"],
    ];

    // TODO : brushup for firebase cloud service
  }

  public setUserItem(name:string, email:string, uid:string){
    this.name = name;
    this.email = email;
    this.uid = uid;
  }
  
  public removeUserItem(){
    this.name = "";
    this.email = "";
    this.uid = "";
  }

  public setEnemyItem(uid:string){
    this.enm_uid = uid;
  }

  public removeEnermyUserItem(){
    this.enm_uid = "";
  }

  public scoreRollback(){
    console.log("finished cancel, do rollback");
    let _len = this.roundScores.length;
    this.roundScores.splice(_len-1, 1);
    this._scoreRefresh();
  }

  public startMatch(matchId:number){
    this.roundScores = [];
    this.roundUsedDarts = 0;
    this.enm_roundScores = [];
    this.roundAveragePPR = 0.0;
    this.highstPPR = 0;
    this.lowestPPR = 0;
    this.currentOutSuccess = 0;
    this.currentOutChance = 0;
    this.currentTonOutSuccess = 0;
    this.currentTonOutChance = 0;
    this.roundTon = 0;
    this.roundTon80 = 0;
    this.currentScore = this.zeroOne;
    this.outScore = 0;
    this.resultData = [];
    this._winCount = 0;
    this._loseCount = 0;
  }
  public resetAll(matchId:number){
    this.roundScores = [];
    this.roundUsedDarts = 0;
    this.roundAveragePPR = 0.0;
    this.matchAveragePPR = 0.0;
    this.highstPPR = 0;
    this.lowestPPR = 0;
    this.currentWinCount = 0;
    this.currentLoseCount = 0;
    this.highOut = 0;
    this.outSuccess = 0;
    this.outChance = 0;
    this.outSuccessPercent = 0.0;
    this.tonOutSuccess = 0;
    this.tonOutChance = 0;
    this.tonOutSuccessPercent = 0.0;
    this.currentOutSuccess = 0;
    this.currentOutChance = 0;
    this.currentTonOutSuccess = 0;
    this.currentTonOutChance = 0;
    this.roundTon = 0;
    this.matchTon = 0;
    this.roundTon80 = 0;
    this.matchTon80 = 0;
    this.currentScore = this.zeroOne;
    this.outScore = 0;
    this.resultData = [];

    this._winCount = 0;
    this._loseCount = 0;
  }
}
