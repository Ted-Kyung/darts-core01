import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, Events, MenuController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';

// restApi services 
import { FirebaseRestApiServices } from "../services/fbRestApi";

const EVENT_UPDATE_USER_DATA = "userDataUpade";

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;

  pages: Array<{title: string, component: any}>;

  // user cont handler
  user1Id = 'user1';
  user2Id = 'user2';
  user1 = null;
  user2 = null;
  // account component handler
  createAccount

  constructor(public platform: Platform, 
              public statusBar: StatusBar, 
              public splashScreen: SplashScreen,
              public events: Events,
              public menu: MenuController,
              public restApi: FirebaseRestApiServices) {
    this.initializeApp();
    // used for an example of ngFor and navigation
  }
  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.statusBar.overlaysWebView(false);
      this.statusBar.show();
      this.registEventSubscribe();
    });
  }

  private async _setUserSummaryData(uid:string){
    let _report = await this.restApi.restApiWithData("getUserSummaryByUid", {"uid":uid});
    if(!!_report){
      console.log("user summary data : " + JSON.stringify(_report));
      return _report;
    }
    console.warn("failed to get user summary data");
    return null;
  }

  registEventSubscribe(){
    console.log("registEventSubscribe");

    this.events.subscribe("findUser1Result", async (result)=>{
      console.log("sidebar recv user1 : " + JSON.stringify(result));
      if(result){
        let _summary = await this._setUserSummaryData(result.uid);
        if(!!_summary){
          console.log("set user1 summary: " + JSON.stringify(_summary));
          this.user1 = _summary.result;
          // publish event "ChangeUser1"
          this.events.publish("ChangedUser1", result);
        }
      } else {
          this.user1 = null;
      }
    });
    this.events.subscribe("findUser2Result", async (result)=>{
      console.log("sidebar recv user2 : " + JSON.stringify(result));
      if(result){
        let _summary = await this._setUserSummaryData(result.uid);
        if(!!_summary){
          console.log("set user2 summary: " + JSON.stringify(_summary));
          this.user2 = _summary.result;
          // publish event "ChangeUser1"
          this.events.publish("ChangedUser2", result);
        }      
      } else {
        this.user2 = null;
        this.events.publish("ChangedUser2", result);
      }
    });
    this.events.subscribe(EVENT_UPDATE_USER_DATA, async (result)=>{
      if(result.user1.uid.length){
        console.log("NNNNNNNNN update user1");
        let _summary = await this._setUserSummaryData(result.user1.uid);
        this.user1 = _summary.result;
      } 
      if(result.user2.uid.length){
        console.log("NNNNNNNNN update user2");
        let _summary = await this._setUserSummaryData(result.user2.uid);
        this.user2 = _summary.result;
      } 
    });

    this.events.subscribe("user1SummaryClick", async (result)=>{
      this.menu.close();
    });
    this.events.subscribe("user2SummaryClick", async (result)=>{
      this.menu.close();
    });
    this.events.subscribe("user1ChangeClick", async (result)=>{
      this.menu.close();
    });
    this.events.subscribe("user2ChangeClick", async (result)=>{
      this.menu.close();
    });
    document.addEventListener('admob.interstitial.events.CLOSE', (event) => {
      console.log("NNNN admob close");
      this.statusBar.overlaysWebView(false);
      this.statusBar.show();
    });
    document.addEventListener('admob.interstitial.events.OPEN', (event) => {
      console.log("NNNN admob close");
      this.statusBar.overlaysWebView(true);
      this.statusBar.hide();
    });
  }

  clickUserChange(data){
    console.log("clickUserChange : " + JSON.stringify(data));
  }

  clickUserSummary(data){
    console.log("clickUserSummary : " + JSON.stringify(data));
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  menuOpen(){
    this.events.publish("menuOpen");
  }

  menuClose(){
    this.events.publish("menuClose");
  }

  clickCreateAccount(){
    this.menu.close();
    this.events.publish("createAccount");
    console.log("click create Account");
  }

  clickFindUser(type:string){
    this.menu.close();
    this.events.publish(type, {user1:this.user1, user2: this.user2});
    console.log("click find user type : " + type);
  }

  clickOneLineNotice(){
    console.log("click one line notice");
    this.menu.close();
    this.events.publish("oneLineNotice", {});
  }

  clickChangeMode(){
    console.log("click change mode");
    this.menu.close();
    this.events.publish("changeMode", {});
  }
}
