import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
// component
import { ComponentsModule } from '../components/components.module';

// plugins
// http
import { HTTP } from '@ionic-native/http';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';

// plugin graph
import { ChartsModule } from 'ng2-charts';

//plugins
// storage
import { IonicStorageModule } from '@ionic/storage';

//plugins
//version
import { AppVersion } from '@ionic-native/app-version';

//services
import { FirebaseLoginService } from '../services/fbLoginService';
import { FirebaseRestApiServices } from '../services/fbRestApi';
import { LangServices } from '../services/languageService';
import { LocalStorageService } from '../services/lcStorageService';

//firebase app 
import { Firebase } from '@ionic-native/firebase/ngx';
import * as firebase from 'firebase/app';

import { AdMobFree } from '@ionic-native/admob-free';


// Initialize Firebase
const firebaseAuthentication = {
  apiKey: "AIzaSyADCVuRiN29IdQqxGlDkNuBJTxIvx_LEw0",
  authDomain: "darts-core01.firebaseapp.com",
  databaseURL: "https://darts-core01.firebaseio.com",
  projectId: "darts-core01",
  storageBucket: "darts-core01.appspot.com",
  messagingSenderId: "408546401443"
};
firebase.initializeApp(firebaseAuthentication);

@NgModule({
  declarations: [
    MyApp,
    HomePage
  ],
  imports: [
    BrowserModule,
    ComponentsModule,
    HttpModule,
    HttpClientModule,
    ChartsModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    FirebaseLoginService,
    FirebaseRestApiServices,
    LangServices,
    LocalStorageService,
    Firebase,
    HTTP,
    AppVersion,
    AdMobFree,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}